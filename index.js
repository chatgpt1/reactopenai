// import { Configuration, OpenAIApi } from "openai";
require('dotenv').config();
const dotenv = require('dotenv')
const OpenAI = require('openai');
const { Configuration, OpenAIApi } = OpenAI;

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 3001;

const configuration = new Configuration({
    organization: "org-308HfWEXkkuLEC8yFg1OQvIK",
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);
// const response = await openai.listEngines();

app.use(bodyParser.json());
app.use(cors());

app.post('/', async (req, res) => {
    const response = await openai.createCompletion({
        model: "text-davinci-003",
        prompt: "Say this is a test",
        max_tokens: 7,
        temperature: 0
    });
    console.log(response.data)
    if (response.data.choices[0].text) {
        res.json({message: response.data.choices[0].text})
    }
});

app.listen(port, () => {
    console.log(`example app listen on http://localhost:${port}/`)
});